#include <math.h>
#include <gtk/gtk.h>

// typedef with all the needed widgets
typedef struct {
    GtkWidget *g_btn_0;
    GtkWidget *g_btn_1;
    GtkWidget *g_btn_2;
    GtkWidget *g_btn_3;
    GtkWidget *g_btn_4;
    GtkWidget *g_btn_5;
    GtkWidget *g_btn_6;
    GtkWidget *g_btn_7;
    GtkWidget *g_btn_8;
    GtkWidget *g_btn_9;
    GtkWidget *g_btn_reset;
    GtkWidget *g_btn_dec;

    GtkWidget *main_dialog;

    GtkWidget *g_lbl_result;

} mainwidgt;

GtkWidget *g_lbl_result;
GtkWidget *main_dialog;

// global vars
float num1 = 0.0f;
float num2 = 0.0f;
float result = 0.0f;
char symbol = ';';
gboolean clear = FALSE;
gboolean symbol_act = FALSE;

int main(int argc, char *argv[]) {

    GtkBuilder      *builder; 
    GtkWidget       *window;
    mainwidgt       *widgets = g_slice_new(mainwidgt);

    gtk_init(&argc, &argv);

    builder = gtk_builder_new_from_file("glade/window_main.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    
    gtk_builder_connect_signals(builder, widgets);

    g_lbl_result = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_result"));
    main_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "main_dialog"));

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
    g_slice_free(mainwidgt, widgets);

    return EXIT_SUCCESS;
}

void update(float args) {
    for (int i=0;i<1;i++) {
        gchar str_num2[256];
        sprintf(str_num2, "%.0f", args);

        const gchar *label_data = gtk_label_get_text(GTK_LABEL(g_lbl_result));

        if (symbol_act == TRUE) {
            char buffer[256];
            strncpy(buffer, label_data, sizeof(buffer));
            strncat(buffer, str_num2, sizeof(buffer));

            gtk_label_set_text(GTK_LABEL(g_lbl_result), buffer);

        } else {
            gchar str_num1[256];
            sprintf(str_num1, "%.0f", args);
            
            char buf[256];
            strncpy(buf, label_data, sizeof(buf));
            strncat(buf, str_num1, sizeof(buf));

            gtk_label_set_text(GTK_LABEL(g_lbl_result), buf);
        }
    }
}

// for about button info
void on_btn_help_activate(void) {
    gtk_dialog_run(GTK_DIALOG(main_dialog));
    gtk_widget_hide(main_dialog);
}

// called when window is closed
void on_window_main_destroy(void) {
    gtk_main_quit();
}

void on_btn_0_clicked(void) { 
    update(0);

    g_print("[*] Pressed %d\n", 0);

}

void on_btn_1_clicked(void) {
    update(1);

    g_print("[*] Pressed %d\n", 1);
}

void on_btn_2_clicked(void) {
    update(2);

    g_print("[*] Pressed %d\n", 2);
}

void on_btn_3_clicked(void) {
    update(3);

    g_print("[*] Pressed %d\n", 3);
}

void on_btn_4_clicked(void) {
    update(4);

    g_print("[*] Pressed %d\n", 4);
}

void on_btn_5_clicked(void) {
    update(5);

    g_print("[*] Pressed %d\n", 5);
}

void on_btn_6_clicked(void) {
    update(6);

    g_print("[*] Pressed %d\n", 6);
}

void on_btn_7_clicked(void) {
    update(7);

    g_print("[*] Pressed %d\n", 7);
}

void on_btn_8_clicked(void) {
    update(8);

    g_print("[*] Pressed %d\n", 8);
}

void on_btn_9_clicked(void) {
    update(9);

    g_print("[*] Pressed %d\n", 9);
}

void on_btn_reset_clicked(void) {
    num1 = 0;
    num2 = 0;
    symbol_act = FALSE;
    g_print("[*] RESET\n");

    gtk_label_set_text(GTK_LABEL(g_lbl_result), "");
}

void text_data(void) {
    const gchar *label_data = gtk_label_get_text(GTK_LABEL(g_lbl_result));
    gint i = atoi(label_data);

    switch (symbol_act) {
        case TRUE :
            num2 = i;
            g_print("[*] TEST NUM2 = %.1f\n",num2);
            break;
        case FALSE :
            num1 = i;
            g_print("[*] TEST NUM1 = %.1f\n",num1);
            break;
        default:
            g_print("[*] ERROR!");
    }
}

void on_btn_add_clicked(GtkLabel *label, mainwidgt *widge) {
    text_data();

    clear = TRUE;
    symbol_act = TRUE;
    symbol = '+';

    g_print("[*] ADDITION SELECTED\n");

    if (clear == TRUE) {
        gtk_label_set_text(GTK_LABEL(g_lbl_result), "");
        clear = FALSE;
    }
}

void on_btn_times_clicked(void) {
    text_data();
    
    clear = TRUE;
    symbol_act = TRUE;
    symbol = 'x';

    g_print("[*] MULTIPLICATION SELECTED\n");

    if (clear == TRUE) {
	    gtk_label_set_text(GTK_LABEL(g_lbl_result), "");
        clear = FALSE;
        //symbol_act = FALSE;

	}
}

void on_btn_div_clicked(void) {
    text_data();
    
    clear = TRUE;
    symbol_act = TRUE;
    symbol = '/';

    g_print("[*] DIVISION SELECTED\n");

    if (clear == TRUE) {
	    gtk_label_set_text(GTK_LABEL(g_lbl_result), "");
        clear = FALSE;
        //symbol_act = FALSE;
	}
}

void on_btn_sub_clicked(void) {
    text_data();
    
    clear = TRUE;
    symbol_act = TRUE;
    symbol = '-';

    g_print("[*] SUBTRACTION SELECTED\n");

    if (clear == TRUE) {
	    gtk_label_set_text(GTK_LABEL(g_lbl_result), "");//l
        clear = FALSE;
        //symbol_act = FALSE;
	}
}

void on_btn_answ_clicked(GtkLabel *label, mainwidgt *widge) {
    symbol_act = TRUE;

    text_data();
    
    switch (symbol) {
        case '+' :
		    g_print("[*] num 1 is %.0f\n", num1);
            g_print("[*] num 2 is %.0f\n", num2);
            
            text_data();

            gchar str_result[256];

            result = num1 + num2;
        	float roundp = roundf(result * 100) / 100;

            sprintf(str_result, "%.0f", roundp);
        
        	gtk_label_set_text(GTK_LABEL(g_lbl_result), str_result);
            symbol_act = FALSE;
		    break;
	    case 'x' :
            g_print("[*] num 1 is %.0f\n", num1);
            g_print("[*] num 2 is %.0f\n", num2);
            
            text_data();

            result = num1 * num2;
            float roundx = roundf(result * 100) / 100;

            sprintf(str_result, "%.0f", roundx);

            gtk_label_set_text(GTK_LABEL(g_lbl_result), str_result);
            symbol_act = FALSE;
            break;
        case '/' :
            g_print("[*] num 1 is %.1f\n", num1);
            g_print("[*] num 2 is %.1f\n", num2);
            
            text_data();

            result = num1 / num2;
            float roundd = roundf(result * 100) / 100;

            sprintf(str_result, "%.2f", roundd);

            gtk_label_set_text(GTK_LABEL(g_lbl_result), str_result);
            symbol_act = FALSE;
            break;
        case '-' :
            g_print("[*] num 1 is %.0f\n", num1);
            g_print("[*] num 2 is %.0f\n", num2);
            
            text_data();

            result = num1 - num2;
            float roundm = roundf(result * 100) / 100;

            sprintf(str_result, "%.0f", roundm);

            gtk_label_set_text(GTK_LABEL(g_lbl_result), str_result);
            symbol_act = FALSE;
            break;
        default:
            g_print("[*] Error!");
    }   
}